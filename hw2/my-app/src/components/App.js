import React from 'react';
import Card from './Card'
import '../styles/main.scss'

class App extends React.Component {
    state = {
        data: [],
        f: true
    }

    async showPr() {
        let response = await fetch('./fruits.json');
        let pr = await response.json();

        this.setState({
            data: pr
        })



    }


    componentDidMount() {
        this.showPr();

    }

    render() {

        return (
            <div className={'card-wrapper'}>
                {this.state.data.map((i, index) =>
                    <Card ifav={this.state.f} key={index} fav={i.infav} name={i.name} price={i.price} code={i.code} color={i.color}
                          path={i.path}/>
                )}
            </div>
        )
    }
}

export default App