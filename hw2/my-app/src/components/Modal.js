import React from "react";
import '../styles/main.scss'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTimes} from '@fortawesome/free-solid-svg-icons'
import PropTypes from "prop-types";
import Btn from "./Btn";

class Modal extends React.Component {
    state = {
        isBlock: false
    }

    off = (e) => {
        if (e.target.classList.contains('modal-bg') ) {
            this.setState({
                isBlock: !this.state.isBlock
            })
        }
    }

    render() {
        return (
            <div className={`modal-bg ${this.state.isBlock ? 'dn' : ''}`} onClick={this.off}>
                <div className={'modal-window'} style={{backgroundColor: this.props.bgc}}>
                    <header className={'modal-header'} style={{backgroundColor: this.props.bgc}}>
                        <h3 className={'header-txt-ml'}>{this.props.htxt}</h3>
                        {this.props.closebutton ? <FontAwesomeIcon icon={faTimes} className={'header-txt-mr'} onClick={this.off}/> : ''}

                    </header>
                    <div className={'modal-msg'}>{this.props.msg}</div>
                    <div className={'modal-btn-block'}>
                        {this.props.actions[0]}
                        {this.props.actions[1]}
                    </div>
                </div>
            </div>
        )
    }
}


Btn.Modal = {
    htxt: PropTypes.string,
    bgc: PropTypes.string,
    closebutton: PropTypes.string,
    actions: PropTypes.string,
}

export default Modal