import React, {Component} from 'react';
import BtnMain from "./BtnMain";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import PropTypes from 'prop-types';

class Card extends Component {
    state = {
        isFav: this.props.fav
    }

    addToFav(name, e) {
        this.setState({
            isFav: !this.state.isFav
        })

        if (this.state.isFav) {
            localStorage.setItem('fav' + name, name);
        } else {
            localStorage.removeItem('fav' + name);
        }
    }

    componentDidMount() {
        if (localStorage.getItem('fav' + this.props.name)) {
            this.setState({
                isFav: !this.state.isFav
            })
        }
    }

    render() {
        const {name, price, code, color} = this.props;

        return (
            <div className={'p-card'}>

                <img src={this.props.path} alt="logo" className={'p-photo'}/>
                <FontAwesomeIcon icon={faStar} className={ this.state.isFav ? 'p-star' : 'p-star ps-fav' } onClick={this.addToFav.bind(this, this.props.name)}/>
                <p className={'c-mt pt'}>{name}</p>
                <p className={' pt'}>{price}</p>
                <p className={' pt'}>{code}</p>
                <p className={' pt'}>{color}</p>
                <BtnMain product={name} class={'c-btn'} color={'blue'} name={'Add to card'} ok={this.ok}/>

            </div>
        );
    }
}

Card.propTypes = {
    isFav: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.string,
    code: PropTypes.string,
}

export default Card;