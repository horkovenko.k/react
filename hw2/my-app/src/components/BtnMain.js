import React from "react";
import '../styles/main.scss'
import Btn from "./Btn";
import Modal from "./Modal";

class BtnMain extends React.Component {
    state = {
        isModal: false
    }

    create() {
        return (
            <Modal htxt={'Are you sure?'} msg={'You can remove it later any time!'} bgc={'green'}
                    actions={[<Btn name={'Ok'} color={'blue'} fn={this.changeM}/>, <Btn name={'Cancel'} color={'blue'} fn={this.changeM}/>]}
            />
        )
    }

    changeM = () => {
        this.setState({
            isModal: !this.state.isModal
        })
    };

    changeMk = (name) => {
        this.setState({
            isModal: !this.state.isModal
        })
        localStorage.setItem('basket' + name, name);
    };

    render() {
        return (
            <div>
                <Btn  class={'c-btn'} color={'blue'} name={'Add to card'} fn={this.changeM}/>
                { this.state.isModal && <Modal htxt={'Are you sure?'} msg={'You can remove it later any time!'} bgc={'green'}
                                               actions={[<Btn name={'Ok'} color={'blue'} fn={this.changeMk.bind(this, this.props.product)}/>, <Btn name={'Cancel'} color={'blue'} fn={this.changeM}/>]}
                /> }
            </div>
        )
    }
}

export default BtnMain