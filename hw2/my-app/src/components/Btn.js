import React from "react";
import '../styles/main.scss'
import PropTypes from "prop-types";

class Btn extends React.Component{

    render() {
        return (
            <button cart={this.props.product} className={`btn ${this.props.class}`} onClick={this.props.fn} style={{backgroundColor: this.props.color}}>{this.props.name}</button>
        )
    }
}

Btn.propTypes = {
    cart: PropTypes.string,
    class: PropTypes.string,
    color: PropTypes.string,
    name: PropTypes.string,
}


export default Btn