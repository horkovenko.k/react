import React from "react";
import '../styles/main.scss'
import Btn from "./Btn";
import Modal from "./Modal";

class BtnMainSecond extends React.Component {
    state = {
        isModal: false
    }

    create() {
        return (
            <Modal htxt={'Open this file?'} msg={'Ok continue sure?'} bgc={'gold'} closebutton={'true'}
                    actions={[<Btn name={'Ok'} color={'green'} fn={this.changeM}/>, <Btn name={'Cancel'} color={'red'} fn={this.changeM}/>]}
            />
        )
    }

    changeM = () => {
        this.setState({
            isModal: !this.state.isModal
        })
    }


    render() {
        return (
            <div>
                <Btn color={'gold'} name={'Modal 2'} fn={this.changeM}/>
                { (this.state.isModal) ? this.create() : '' }


            </div>
        )
    }
}

export default BtnMainSecond