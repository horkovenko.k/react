import React from "react";
import '../styles/main.scss'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTimes} from '@fortawesome/free-solid-svg-icons'

class Modal extends React.Component {
    state = {
        ChangeBgc: false
    }

    off = (e) => {
        if (e.target.classList.contains('modal-bg') ||
            e.target.classList.contains('fa-times')) {
            this.setState({
                ChangeBgc: !this.state.ChangeBgc
            })
        }
    }

    render() {
        return (
            <div className={`modal-bg ${this.state.ChangeBgc ? 'dn' : ''}`} onClick={this.off}>
                <div className={'modal-window'} style={{backgroundColor: this.props.bgc}}>
                    <header className={'modal-header'} style={{backgroundColor: this.props.bgc}}>
                        <h3 className={'header-txt-ml'}>{this.props.htxt}</h3>
                        {this.props.closebutton ? <FontAwesomeIcon icon={faTimes} className={'header-txt-mr'} onClick={this.off}/> : ''}

                    </header>
                    <div className={'modal-msg'}>{this.props.msg}</div>
                    <div className={'modal-btn-block'}>
                        {this.props.actions[0]}
                        {this.props.actions[1]}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal