import React from "react";
import '../styles/main.scss'

class Btn extends React.Component{

    render() {
        return (
            <button className={'btn '} onClick={this.props.fn} style={{backgroundColor: this.props.color}}>{this.props.name}</button>
        )
    }
}

export default Btn