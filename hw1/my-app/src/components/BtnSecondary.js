import React from "react";
import '../styles/main.scss'
import Btn from "./Btn";
import Modal from "./Modal";

class BtnSecondary extends React.Component {
    state = {
        isModal: false
    }

    create() {
        return (
            <Modal htxt={'Open this file?'} msg={'Ok continue sure?'} bgc={'gold'} closebutton={'true'}
                    actions={[<Btn name={'Ok'} color={'green'} fn={this.changeModalBackground}/>, <Btn name={'Cancel'} color={'red'} fn={this.changeModalBackground}/>]}
            />
        )
    }

    changeModalBackground = () => {
        this.setState({
            isModal: !this.state.isModal
        })
    }


    render() {
        return (
            <div>
                <Btn color={'gold'} name={'Modal 2'} fn={this.changeModalBackground}/>
                { (this.state.isModal) ? this.create() : '' }


            </div>
        )
    }
}

export default BtnSecondary