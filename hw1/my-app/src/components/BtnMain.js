import React from "react";
import '../styles/main.scss'
import Btn from "./Btn";
import Modal from "./Modal";

class BtnMain extends React.Component {
    state = {
        isModal: false
    }

    create() {
        return (
            <Modal htxt={'Close this window?'} msg={'Are you sure?'} bgc={'red'}
                    actions={[<Btn name={'Ok'} color={'red'} fn={this.changeModalBackground}/>, <Btn name={'Cancel'} color={'green'} fn={this.changeModalBackground}/>]}
            />
        )
    }

    changeModalBackground = () => {
        this.setState({
            isModal: !this.state.isModal
        })
    }


    render() {
        return (
            <div>
                <Btn color={'red'} name={'Modal 1'} fn={this.changeModalBackground}/>
                { (this.state.isModal) ? this.create() : '' }


            </div>
        )
    }
}

export default BtnMain