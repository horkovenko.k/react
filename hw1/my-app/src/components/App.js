import React from 'react';
import BtnMain from './BtnMain'
import BtnSecondary from './BtnSecondary'
import '../styles/main.scss'

class App extends React.Component {

    render() {
       return (
           <div>
               <BtnMain />
               <BtnSecondary />
           </div>
       )
    }
}

export default App