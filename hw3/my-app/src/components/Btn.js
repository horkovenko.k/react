import React from "react";
import '../styles/main.scss'

function Btn(props) {

    return (
        <button cart={props.product} className={`btn ${props.class}`} onClick={props.fn}
                style={{backgroundColor: props.color}}>{props.name}</button>
    )
}


export default Btn