import React, { useEffect, useState} from 'react';
import Card from "./Card";

function Favorites() {
    useEffect(() => {
        showPr();

    }, []);
    const [data, setData] = useState([]);
    const [f] = useState(true);

    async function showPr() {
        let response = await fetch('./fruits.json');
        let pr = await response.json();

        setData(pr)

    }

    function isHere(i) {
        if (localStorage.getItem('fav' + data[i].name)) {
            return true;
        }
    }


    return (
        <div className={'cd-wrap'}>
            {data.map((i, index) => isHere(index)
                ?
                <Card ifav={f} key={index} fav={i.infav} name={i.name} price={i.price} code={i.code} color={i.color}
                        path={i.path}/> : ''

            )}
        </div>
    )

}

export default Favorites;