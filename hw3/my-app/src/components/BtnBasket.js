import React, {useState} from "react";
import '../styles/main.scss'
import Btn from "./Btn";
import Modal from "./Modal";



function BtnMain(props) {
    const [isModal, setIsModal] = useState(false);



    let changeModal = () => {
        setIsModal(!isModal);
    };

    let changeModalKey = (name) => {
        setIsModal(!isModal);

        localStorage.removeItem('basket' + name);
    };

    
        return (
            <div>
                <Btn  class={'c-btn'} color={'red'} name={'Delete'} fn={changeModal}/>
                { isModal && <Modal htxt={'Are you sure?'} msg={'You can add it later any time!'} bgc={'red'}
                                               actions={[<Btn name={'Ok'} color={'blue'} class={'btn-basketOk'} fn={changeModalKey.bind(this, props.product)}/>, <Btn name={'Cancel'} color={'blue'} fn={changeModal}/>]}
                /> }
            </div>
        )

}

export default BtnMain