import React from "react";
import Home from "./Home";
import Favorites from "./Favorites";
import Basket from "./Basket";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

export default function App() {


    return (
        <Router>
            <div className={'main-shop-wrap'}>
                <nav>
                    <ul className={'shop-nav'}>
                        <li className={'link'}>
                            <Link to="/">Home</Link>
                        </li>
                        <li className={'link'}>
                            <Link to="/favorites">Favorites</Link>
                        </li>
                        <li className={'link'}>
                            <Link to="/basket">Basket</Link>
                        </li>
                    </ul>
                </nav>

                {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                <Switch>
                    <Route path="/favorites">
                        <Favorites/>
                    </Route>
                    <Route path="/basket">
                        <Basket/>
                    </Route>
                    <Route path="/">
                        <Home/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );

}

