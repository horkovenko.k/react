import React, { useState } from "react";
import '../styles/main.scss'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTimes} from '@fortawesome/free-solid-svg-icons'


function Modal(props) {
    // state = {
    //     isBB: false
    // }
    const [isBB, setisBB] = useState(false);

    let off = (e) => {
        if (e.target.classList.contains('modal-bg') ) {
            // this.setState({
            //     isBB: !this.state.isBB
            // })
            setisBB(!isBB)
        }
    }

        return (
            <div className={`modal-bg ${isBB ? 'dn' : ''}`} onClick={off}>
                <div className={'modal-window'} style={{backgroundColor: props.bgc}}>
                    <header className={'modal-header'} style={{backgroundColor: props.bgc}}>
                        <h3 className={'header-txt-ml'}>{props.htxt}</h3>
                        {props.closebutton ? <FontAwesomeIcon icon={faTimes} className={'header-txt-mr'} onClick={off}/> : ''}

                    </header>
                    <div className={'modal-msg'}>{props.msg}</div>
                    <div className={'modal-btn-block'}>
                        {props.actions[0]}
                        {props.actions[1]}
                    </div>
                </div>
            </div>
        )
}

export default Modal