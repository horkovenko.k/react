import React, { useState, useEffect} from 'react';
import Card from "./Card";

function Home() {
    useEffect(() => {
        showPr();
    }, []);
    const [data, setData] = useState([]);
    const [f] = useState(true);


    async function showPr() {
        let response = await fetch('./fruits.json');
        let pr = await response.json();

        setData(pr)

    }



        return (
            <div className={'cd-wrap'}>
                {data.map((i, index) =>
                    <Card ifav={f} key={index} fav={i.infav} name={i.name} price={i.price} code={i.code} color={i.color}
                          path={i.path}/>
                )}
            </div>
        )

}

export default Home;