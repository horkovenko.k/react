import React, {useState} from "react";
import '../styles/main.scss'
import Btn from "./Btn";
import Modal from "./Modal";

function BtnMain(props) {
    const [isModal, setIsModal] = useState(false);

    let changeM = () => {
        setIsModal(!isModal);
    };

    let changeMk = (name) => {
        setIsModal(!isModal);

        localStorage.setItem('basket' + name, name);
    };

        return (
            <div>
                <Btn  class={'c-btn'} color={'blue'} name={'Add to card'} fn={changeM}/>
                { isModal && <Modal htxt={'Are you sure?'} msg={'You can remove it later any time!'} bgc={'green'}
                                               actions={[<Btn name={'Ok'} color={'blue'} fn={changeMk.bind(this, props.product)}/>, <Btn name={'Cancel'} color={'blue'} fn={changeM}/>]}
                /> }
            </div>
        )

}

export default BtnMain