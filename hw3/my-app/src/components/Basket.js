import React, { useEffect, useState} from 'react';
import CardBt from "./CardBt";

function Basket(props) {
    useEffect(() => {
        showPr();

    }, []);
    const [data, setData] = useState([]);
    const [f] = useState(true);
    const [vis, setVis] = useState(true);
///btn-basketOk
    async function showPr() {
        let response = await fetch('./fruits.json');
        let pr = await response.json();

        setData(pr)
    }

    function isHere(i) {
        if (localStorage.getItem('basket' + data[i].name)) {
            return true;
        }
    }

    return (
        <div className={'cd-wrap'}>
            {window.addEventListener('click', (e) => {
                if (e.target.classList.contains('btn-basketOk')) {
                    setVis(!vis);
                }})}

            {data.map((i, index) => isHere(index)
                ?
                <CardBt isVisible={true} ifav={f} key={index} fav={i.infav} name={i.name} price={i.price} code={i.code} color={i.color}
                      path={i.path}/> : ''

            )}
        </div>
    )

}

export default Basket;