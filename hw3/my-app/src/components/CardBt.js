import React, { useState, useEffect} from 'react';
import BtnBasket from "./BtnBasket";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faStar} from '@fortawesome/free-solid-svg-icons'

function CardBt(props) {

    const [isFav, setIsFav] = useState(props.fav);

    function addToFav(name, e) {

        setIsFav(!isFav)

        if (isFav) {
            localStorage.setItem('fav' + name, name);
        } else {
            localStorage.removeItem('fav' + name);
        }
    }

    useEffect(() => {
        if (localStorage.getItem('fav' + props.name)) {
            setIsFav(!isFav)
        }

    },[]);

    return (
        <div className={`p-card ${props.isVisible ? '' : 'dn'}`}>

            <img src={props.path} alt="logo" className={'p-photo'}/>
            <FontAwesomeIcon icon={faStar} className={isFav ? 'p-star' : 'p-star ps-fav'}
                             onClick={addToFav.bind(this, props.name)}/>
            <p className={'c-mt pt'}>{props.name}</p>
            <p className={' pt'}>{props.price}</p>
            <p className={' pt'}>{props.code}</p>
            <p className={' pt'}>{props.color}</p>
            <BtnBasket class={'ok-basket'}   product={props.name} ok={props.ok}/>

        </div>
    );

}

export default CardBt;