export let GET_PRODUCTS = "GET_PRODUCTS";
export let SWITCH_MODAL = "SWITCH_MODAL";

export let MODULE_NAME = "products";
export let MODULE_NAME2 = "products2";

export const selectProductsList = state => state[MODULE_NAME].list;
export const selectModal = state => state[MODULE_NAME2].isModalOpen;

export const initialState = {
    list: [],
    isModalOpen: true
};

export function reducer(state = initialState, {type, payload}) {
    switch (type) {
        case GET_PRODUCTS:
            return {
                ...state,
                list: payload
            };

        case SWITCH_MODAL:
            return {
                ...state,
                isModalOpen: payload
            };

        default:
            return state;
    }
}
