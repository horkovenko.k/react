import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {reducer as productsReducer, MODULE_NAME as productsModuleName, MODULE_NAME2 as productsModuleName2} from './action'
// import {reducer as modalReducer, MODULE_NAME as modalModuleName} from './modals';
import {reducer as formReducer} from 'redux-form';

const rootReducer = combineReducers({
    [productsModuleName]: productsReducer,
    [productsModuleName2]: productsReducer,
    form: formReducer
});

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;