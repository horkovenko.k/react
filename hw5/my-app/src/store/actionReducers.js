import {GET_PRODUCTS, SWITCH_MODAL, initialState} from "./action";

const setProducts = (payload) => ({
    type: GET_PRODUCTS,
    payload
});

const setModalState = (payload) => ({
    type: SWITCH_MODAL,
    payload
});

export const getProducts = () => async dispatch => {
    let response = await fetch('./fruits.json');
    let fruits = await response.json();
    dispatch(setProducts(fruits));
};

export const getModalState = () => dispatch => {
    dispatch(setModalState(initialState.isModalOpen = !initialState.isModalOpen));
};