import React from "react";
import Btn from "./Btn";
import {shallow} from "react-test-renderer";
import {expect, toMatchSnapshot} from "enzyme"
import renderer from "react-test-renderer";

it("render correctly ButtonSimpleTemplate component", () => {
    const BtnEvery = renderer.create(<Btn/>).toJSON();
    expect(BtnEvery).toMatchSnapshot();
});

it("check ButtonSimpleTemplate has default class", () => {
        const Btn = mount(<Btn {...props} />).find(
            ".btn"
        );
});

it("render ButtonSimpleTemplate input correctly with null value", () => {
    const props = {
            name: null
        },
        Btn = mount(<Btn {...props} />);
    expect(Btn.prop("name")).toEqual(null);
});

it("check the type of name", () => {
    const props = {
            name: "any"
        },
        Btn = mount(<Btn {...props} />);
    expect(Btn.prop("name")).toBeString();
});