import React, {useState} from "react";
import '../../../styles/main.scss'
import Btn from "../ButtonSimpleTemplate/Btn";
import Modal from "../../Modal/Modal";
import { selectModal, selectProductsList} from "../../../store/action";
import { getModalState } from "../../../store/actionReducers";
import {connect} from "react-redux";

function BtnMain(props) {
    const [isModal, setIsModal] = useState(false);

    const {isModalOpen, getModalState} = props;

    let changeModal = () => {
        getModalState();
        setIsModal(isModalOpen);
    };

    let changeModalKey = (name) => {
        getModalState();
        setIsModal(isModalOpen);
        localStorage.removeItem('basket' + name);
    };

    
        return (
            <div>
                <Btn  class={'c-btn'} color={'red'} name={'Delete'} fn={changeModal}/>
                { isModal && <Modal htxt={'Are you sure?'} msg={'You can add it later any time!'} bgc={'red'}
                                               actions={[<Btn name={'Ok'} color={'blue'} class={'btn-basketOk'} fn={changeModalKey.bind(this, props.product)}/>, <Btn name={'Cancel'} color={'blue'} fn={changeModal}/>]}/> }
            </div>
        )

}

const mapStateToProps = (state) => ({
    list: selectProductsList(state),
    isModalOpen: selectModal(state)
});

export default connect(mapStateToProps, {getModalState})(BtnMain);