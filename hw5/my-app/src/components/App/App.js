import React from "react";
import Home from "../Home/Home";
import Favorites from "../Favorites/Favorites";
import Basket from "../Basket/Basket";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

function App() {


    return (
        <Router>
            <div className={'main-shop-wrap'}>
                <nav>
                    <ul className={'shop-nav'}>
                        <li className={'link'}>
                            <Link to="/">Home</Link>
                        </li>
                        <li className={'link'}>
                            <Link to="/favorites">Favorites</Link>
                        </li>
                        <li className={'link'}>
                            <Link to="/basket">Basket</Link>
                        </li>
                    </ul>
                </nav>

                <Switch>
                    <Route path="/favorites">
                        <Favorites/>
                    </Route>
                    <Route path="/basket">
                        <Basket/>
                    </Route>
                    <Route path="/">
                        <Home/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );

}

export default App;