import React from "react";
import Modal from "./Modal";
import {shallow} from "react-test-renderer";
import {expect, toMatchSnapshot} from "enzyme"

it("without component", () => {
    const Modal = shallow(<Modal/>);
    expect(Modal).toMatchSnapshot();
});

it("with component", () => {
    const props = {
            component: () => {}
        },
        ModalWrapperComponent = shallow(<Modal {...props} />);
    expect(Modal).toMatchSnapshot();
});

it("render correct class name", () => {
    const props = {
            modalClassName: "modal-window"
        },
        ModalWrapperComponent = shallow(<Modal {...props} />).find(
            "Modal"
        );
    expect(ModalWrapperComponent.hasClass("modal-window")).toEqual(true);
});

it("check the modal is opened", () => {
    const event = {
        preventDefault: () => {},
        stopPropagation: () => {}
    };
    ModalTriggerComponent.instance().open(event);
    expect(ModalTriggerComponent.state().toggled).toBeTruthy();
});

it("check the modal is closed", () => {
    ModalTriggerComponent.instance().close();
    expect(ModalTriggerComponent.state().toggled).toBeFalsy();
});