import React, {useEffect, useRef, useState} from 'react';
import Card from "../Card/Card";
import {selectProductsList} from "../../store/action";
import {getProducts} from "../../store/actionReducers";
import {connect} from "react-redux";

function Favorites(props) {
    const {getProducts, list} = props;

    const warning = useRef(null);
    const container = useRef(null);
    const [empty, setEmpty] = useState(false);

    useEffect(() => {
        getProducts();
    }, [getProducts]);

    useEffect(() => {
        if (container.current.children.length <= 1) {
            setEmpty(true);
        } else {
            setEmpty(false);
        }

    }, []);

    const [favourite] = useState(true);

    function isInFavs(i) {
        if (localStorage.getItem('fav' + list[i].name)) {
            return true;
        }
    }

    return (
        <div ref={container} className={'cd-wrap'}>
            {list.map((i, index) => isInFavs(index)
                ?
                <Card ifav={favourite} key={index} fav={i.infav} name={i.name} price={i.price} code={i.code} color={i.color}
                        path={i.path}/> : '')}

            <h2 ref={warning} style={{color: "yellow"}} className={empty ? '' : 'dn'} >No Items have been added!</h2>

        </div>
    )
}

const mapStateToProps = (state) => ({
    list: selectProductsList(state)
});

export default connect(mapStateToProps, {getProducts})(Favorites);