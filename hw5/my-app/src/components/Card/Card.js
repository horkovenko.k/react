import React, { useState, useEffect} from 'react';
import BtnMain from "../AllButtons/BtnMain/BtnMain";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'

function Card(props) {
    const [isFav, setIsFav] = useState(props.fav);

    function addToFav(name, e) {
        setIsFav(!isFav);

        if (isFav) {
            localStorage.setItem('fav' + name, name);
        } else {
            localStorage.removeItem('fav' + name);
        }
    }

    useEffect(() => {
        if (localStorage.getItem('fav' + props.name)) {
            setIsFav(!isFav)
        }
    }, []);

        return (
            <div className={'p-card'}>

                <img src={props.path} alt="logo" className={'p-photo'}/>
                <FontAwesomeIcon icon={faStar} className={ isFav ? 'p-star' : 'p-star ps-fav' } onClick={addToFav.bind(this, props.name)}/>
                <p className={'c-mt pt'}>{props.name}</p>
                <p className={' pt'}>{props.price}</p>
                <p className={' pt'}>{props.code}</p>
                <p className={' pt'}>{props.color}</p>
                <BtnMain product={props.name} class={'c-btn'} color={'blue'} name={'Add to card'} ok={props.ok}/>

            </div>
        );

}

export default Card;