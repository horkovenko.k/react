import React, {useEffect, useState, useRef} from 'react';
import CardBt from "../CardBt/CardBt";
import {connect} from "react-redux";
import SubmitFormBasket from "../Form/SubmitFormBasket"
import {selectProductsList} from "../../store/action";
import {getProducts} from "../../store/actionReducers";
import showResults from "../Form/SubmitType"


function Basket(props) {
    const {getProducts, list} = props;

    const warning = useRef(null);
    const container = useRef(null);
    const [empty, setEmpty] = useState(false);


    useEffect(() => {
        getProducts();

    }, [getProducts]);

    useEffect(() => {
        if (container.current.children.length <= 2) {
            setEmpty(true);
        } else {
            setEmpty(false);
        }

    }, []);

    const [favourite] = useState(true);
    const [visibility, setVisibility] = useState(true);

    function isInBasket(i) {
        if (localStorage.getItem('basket' + list[i].name)) {
            return true;
        }
    }

    function showBasket() {
        return list.map((i, index) => isInBasket(index)
            ?
            <CardBt isVisible={true} ifav={favourite} key={index} fav={i.infav} name={i.name} price={i.price}
                    code={i.code} color={i.color}
                    path={i.path}/> : '')
    }

    return (
        <div ref={container} className={'cd-wrap'}>
            {window.addEventListener('click', (e) => {
                if (e.target.classList.contains('btn-basketOk')) {
                    setVisibility(!visibility);
                }
            })}

            {showBasket()}
            <h2 ref={warning} style={{color: "red"}} className={empty ? '' : 'dn'} >No Items have been added!</h2>


            <SubmitFormBasket onSubmit={showResults}/>
        </div>
    )

}

const mapStateToProps = (state) => ({
    list: selectProductsList(state)
});

export default connect(mapStateToProps, {getProducts})(Basket);