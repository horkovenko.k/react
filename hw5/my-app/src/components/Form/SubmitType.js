// import React from 'react';
//
// const SubmitType = () => {
//     return (
//         <div>
//
//         </div>
//     );
// };
//
// export default SubmitType;

let basket = [];

function clearBasket() {
    for (let i = 0; i < localStorage.length; i++) {
        if (localStorage.key(i).includes('basket')) {
            basket.push(localStorage.getItem(localStorage.key(i)));
        }
    }
    return basket;
}

function clearStorage() {
    for (let i = 0; i < basket.length; i++) {
        localStorage.removeItem('basket' + basket[i]);
    }
}

export default (async function showResults(values) {
    values.basket = clearBasket();
    clearStorage();

    console.log(`You submitted:\n\n${JSON.stringify(values, null, 2)}`);
});
