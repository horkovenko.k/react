import React from 'react';
import '../../styles/main.scss'
import {Field, reduxForm, submit} from 'redux-form'

const required = value => value ? undefined : 'Required'
const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined
const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined
const minLength8 = minLength(8)
const minLength3 = minLength(3)
const maxLength15 = maxLength(15)
const maxLength55 = maxLength(55)
const number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined
const minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined
const minValue18 = minValue(18)


const renderField = ({input, label, type, meta: {touched, error, warning}}) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type}/>
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    </div>
);


let SubmitFormBasket = (props) => {

    const {handleSubmit, reset, submitting, valid} = props;


    return (
        <form name={"basket-form"} onSubmit={handleSubmit} className={"form-borders"}>
            <div className={"input-style"}>
                <label>First Name: </label>
                <Field validate={[required, maxLength15, minLength3]} component={renderField} name="firstName"
                       type="text" placeholder="First Name"/>
            </div>
            <div className={"input-style"}>
                <label>Last Name: </label>
                <Field validate={[required, maxLength15, minLength3]} name="lastName" component={renderField}
                       type="text" placeholder="Last Name"/>
            </div>
            <div className={"input-style"}>
                <label>Age: </label>
                <Field name="age" validate={[required, number, minValue18]} component={renderField} type="text"
                       placeholder="Age"/>
            </div>
            <div className={"input-style"}>
                <label>Address: </label>
                <Field validate={[required, maxLength55, minLength3]} name="address" component={renderField} type="text"
                       placeholder="Address"/>
            </div>
            <div className={"input-style"}>
                <label>Mobile Phone: </label>
                <Field validate={[required, number, maxLength15, minLength8, minLength3]} name="mobilePhone"
                       component={renderField} type="text" placeholder="Mobile Phone"/>
            </div>

            <button name="submit" disabled={!valid || submitting} onClick={function (event) {
                submit();
                setTimeout(reset, 100);
            }} className={'btn bt-sub'}>Checkout
            </button>

        </form>
    )
}

SubmitFormBasket = reduxForm({
    form: 'contact'
})(SubmitFormBasket);

export default SubmitFormBasket;