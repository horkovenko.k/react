let GET_PRODUCTS = "GET_PRODUCTS";
let SWITCH_MODAL = "SWITCH_MODAL";

export let MODULE_NAME = "products";
export let MODULE_NAME2 = "products2";

export const selectProductsList = state => state[MODULE_NAME].list;
export const selectModal = state => state[MODULE_NAME2].isModalOpen;

let initialState = {
    list: [],
    isModalOpen: true
};

export function reducer(state = initialState, {type, payload}) {
    switch (type) {
        case GET_PRODUCTS:
            return {
                ...state,
                list: payload
            };

        case SWITCH_MODAL:
            return {
                ...state,
                isModalOpen: payload
            };

        default:
            return state;
    }
}

const setProducts = (payload) => ({
    type: GET_PRODUCTS,
    payload
});

const setModalState = (payload) => ({
    type: SWITCH_MODAL,
    payload
});

export const getProducts = () => async dispatch => {
    let response = await fetch('./fruits.json');
    let fruits = await response.json();
    dispatch(setProducts(fruits));
};

export const getModalState = () => dispatch => {
    dispatch(setModalState(initialState.isModalOpen = !initialState.isModalOpen));
};