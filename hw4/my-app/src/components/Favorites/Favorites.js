import React, { useEffect, useState} from 'react';
import Card from "../Card/Card";
import {getProducts, selectProductsList} from "../../actions/action";
import {connect} from "react-redux";

function Favorites(props) {
    const {getProducts, list} = props;

    useEffect(() => {
        getProducts();
    }, [getProducts]);
    const [f] = useState(true);

    function isHere(i) {
        if (localStorage.getItem('fav' + list[i].name)) {
            return true;
        }
    }

    return (
        <div className={'cd-wrap'}>
            {list.map((i, index) => isHere(index)
                ?
                <Card ifav={f} key={index} fav={i.infav} name={i.name} price={i.price} code={i.code} color={i.color}
                        path={i.path}/> : '')}
        </div>
    )
}

const mapStateToProps = (state) => ({
    list: selectProductsList(state)
});

export default connect(mapStateToProps, {getProducts})(Favorites);