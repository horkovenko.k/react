import React, { useEffect, useState} from 'react';
import CardBt from "../CardBt/CardBt";
import {connect} from "react-redux";
import {getProducts, selectProductsList} from "../../actions/action";

function Basket(props) {
    const {getProducts, list} = props;

    useEffect(() => {
        getProducts();
    }, [getProducts]);

    const [f] = useState(true);
    const [vis, setVis] = useState(true);

    function isHere(i) {
        if (localStorage.getItem('basket' + list[i].name)) {
            return true;
        }
    }

    return (
        <div className={'cd-wrap'}>
            {window.addEventListener('click', (e) => {
                if (e.target.classList.contains('btn-basketOk')) {
                    setVis(!vis);
                }})}

            {list.map((i, index) => isHere(index)
                ?
                <CardBt isVisible={true} ifav={f} key={index} fav={i.infav} name={i.name} price={i.price} code={i.code} color={i.color}
                      path={i.path}/> : '')}
        </div>
    )

}

const mapStateToProps = (state) => ({
    list: selectProductsList(state)
});

export default connect(mapStateToProps, {getProducts})(Basket);