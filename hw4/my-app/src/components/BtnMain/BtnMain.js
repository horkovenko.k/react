import React, {useState} from "react";
import '../../styles/main.scss'
import Btn from "../Btn/Btn";
import Modal from "../Modal/Modal";
import {getModalState, selectModal, selectProductsList} from "../../actions/action";
import {connect} from "react-redux";

function BtnMain(props) {
    const [isModal, setIsModal] = useState(false);

    const {isModalOpen, getModalState} = props;

    let changeModal = () => {
        getModalState();
        setIsModal(isModalOpen);
    };

    let changeModalKey = (name) => {
        getModalState();
        setIsModal(isModalOpen);
        localStorage.setItem('basket' + name, name);
    };

    return (
        <div>
            <Btn class={'c-btn'} color={'blue'} name={'Add to card'} fn={changeModal}/>
            {isModal && <Modal htxt={'Are you sure?'} msg={'You can remove it later any time!'} bgc={'green'}
                               actions={[<Btn name={'Ok'} color={'blue'}
                                              fn={changeModalKey.bind(this, props.product)}/>,
                                   <Btn name={'Cancel'} color={'blue'} fn={changeModal}/>]}/>}
        </div>
    )

}

const mapStateToProps = (state) => ({
    list: selectProductsList(state),
    isModalOpen: selectModal(state)
});

export default connect(mapStateToProps, {getModalState})(BtnMain);